const request = require('request-promise')
const { performance } = require('perf_hooks')
const auth = require('./auth')

const responseTimes = {
  LOW: 1.5,
  NORMAL: 3.0,
  HIGH: 5.0
}

const clusterTime = deltaTime => {
  const labels = Object.keys(responseTimes).filter(key => (deltaTime / 1000) <= responseTimes[key])

  return labels.length ? labels[0] : 'VERY_HIGH'
}

const requestAuth = async (host, endpoint) => {
  const startTime = performance.now()
  const response = await request({
    scope: endpoint,
    uri: `${host}${endpoint}`,
    method: 'GET',
    headers: {
      Authorization: await auth.token
    },
    json: true
  }).then(data => {
    const endTime = performance.now()
    const deltaTime = endTime - startTime
    return {status: 200, endpoint, deltaTime, clusterTime: clusterTime(deltaTime) }
  }).catch(err => {
    const endTime = performance.now()
    const deltaTime = endTime - startTime
    return { status: err.statusCode, endpoint, deltaTime, clusterTime: clusterTime(deltaTime) }
  })
  return response
}

const requestSimple = async (host, endpoint) => {
  const startTime = performance.now()
  const response = await request({
      scope: endpoint,
      uri: `${host}${endpoint}`,
      method: 'GET',
      json: true
    }).then(data => {
      const endTime = performance.now()
      const deltaTime = endTime - startTime
      return {status: 200, endpoint, data, deltaTime, clusterTime: clusterTime(deltaTime) }
    }).catch(err => {
      const endTime = performance.now()
      const deltaTime = endTime - startTime
      return { status: err.statusCode, endpoint, deltaTime, clusterTime: clusterTime(deltaTime) }
    })
    return response
}

module.exports = {
  requestAuth,
  request: requestSimple
}