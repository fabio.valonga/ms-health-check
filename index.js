const { request, requestAuth } = require('./request')
const { endpoints, host, authenticated, priority } = require('./endpoints.json')
const fs = require('fs')
const auth = require('./auth')

const printErrorsConsole = (name, report) => {
  console.log(`${name} Broken:`)
  report.forEach(({ endpoint, status }) => {
    const broken = status.filter(st => st.status !== 200).map(st => st.status)
    if(broken.length){
      console.log(`[Broken: ${broken.length}/4] - ${endpoint}  status=[${broken}]`)
    }
  })
  console.log('\n-----------------------------------------------------------------------')
  console.log(`${name} High Time:`)
  report.forEach(({ endpoint, status }) => {
    const high = status.filter(st => st.clusterTime === 'VERY_HIGH' || st.clusterTime === 'HIGH')
      .map(({reqTime, clusterTime}) => `${clusterTime}: ${reqTime}ms`)
    if(high.length) console.log(`[Affected: ${high.length}/4] - ${endpoint} \n${JSON.stringify(high)}`)
  })
  console.log('\n-----------------------------------------------------------------------')
}

const checkServicesHealth = async () => {
  const startTime = new Date().toISOString()
  //console.log(`--------------------------- START - ${startTime} --------------------------`)
  const results = await Promise.all(endpoints.map(async endpoint => {
    const [r1, r2, r3, r4] = await Promise.all([
      request(host, endpoint),
      request(host, endpoint),
      request(host, endpoint),
      request(host, endpoint)
    ])

    return { endpoint, responses: [ r1, r2, r3, r4 ]}
  }))
  const report = results.map(({endpoint, responses}) => ({
    endpoint,
    status: responses.map(resp => ({ status: resp.status, reqTime: resp.deltaTime, clusterTime: resp.clusterTime }))
  })).sort()

  printErrorsConsole('Health', report)

  const endTime = new Date().toISOString()
  fs.writeFileSync('output-health.json', JSON.stringify({ startTime, endTime, report }))
  //console.log(`--------------------------- END - ${endTime} --------------------------`)
}

const checkServicesResponses = async () => {
  const bearerToken = await auth.token
  const results = await Promise.all(authenticated.map(async (endpoint) => {
    const [r1, r2, r3, r4] = await Promise.all([
      requestAuth(host, endpoint),
      requestAuth(host, endpoint),
      requestAuth(host, endpoint),
      requestAuth(host, endpoint)
    ])

    return { endpoint, responses: [ r1, r2, r3, r4 ]}
  }))
  const report = results.map(({endpoint, responses}) => ({
    endpoint,
    status: responses.map(resp => ({ status: resp.status, reqTime: resp.deltaTime, clusterTime: resp.clusterTime }))
  })).sort()
  printErrorsConsole('Responses', report)
  printErrorsConsole('Priority', report.filter(end => priority.includes(end.endpoint)))

  fs.writeFileSync('output-responses.json', JSON.stringify({ bearerToken, report }))
}

checkServicesHealth()
checkServicesResponses()
