const { request } = require('nextel-request')
const { host } = require('./endpoints.json')
let bearerToken

const requestToken = async () => request({
  scope: 'getAuth',
  uri: `${host}/auth/v.4.0/login`,
  method: 'POST',
  body: {
    "phone": "11973933660",
    "token": "111111"
  },
  json: true
})

const getToken = async () => {
  if (!bearerToken) {
    const { token } = await requestToken()
    bearerToken = token
    console.log(`Generated token: ${bearerToken}\n`)
  }
  return bearerToken
}

module.exports = {
  get token() { return getToken() }
}