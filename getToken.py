import sys
import requests
import re

def requestSMS(msisdn):
  sms = requests.post('https://mobileapp-v2.nextel.com.br/auth/v.4.0/access/sms', data={'phone': msisdn})
  if(sms.status_code != 200):
    print('Auth-SMS error')
    exit(1)

def getAuthTokenSession():
  headers= {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie': 'rxVisitor=1582117867599QVJV036MG0883E34N0755N309J5O2EC1'
   # 'Cookie': 'session=eyJjaGVjayI6ImQ0MWQ4Y2Q5OGYwMGIyMDRlOTgwMDk5OGVjZjg0MjdlIn0.Xqh4Ig.Hkx8F2cs1AHnwHC5CvC5mKSj-64'
  }
  data = { 'user': '', 'senha': '' }
  session = requests.Session()
  r = session.post('http://authtoken.nexteldigital.com.br/login', headers=headers, data=data)
  if(r.status_code != 200):
    print('AuthToken offline')
    exit(1)
  return session

def searchAuthToken(session, msisdn):
  regex = r"AuthToken: <strong>(\d+\d+\d+\d+\d+\d+)<\/strong>"
  r = session.post('http://authtoken.nexteldigital.com.br/new/search/', data={'number': msisdn, 'prod': 'edit'})
  matches = re.search(regex, str(r.content), re.MULTILINE)
  return matches.group(1)

def requestBearerToken(msisdn, token):
  bearerToken = requests.post('https://mobileapp-v2.nextel.com.br/auth/v.4.0/login', data={'phone': msisdn, 'token': token})
  if(bearerToken.status_code != 200):
    print('Auth-Login error')
    exit(1)
  print(bearerToken)
  return bearerToken

def main(msisdn):
  print(f'Msisdn={msisdn}')
  print('1) Requesting SMS')
  requestSMS(msisdn)
  print('2) Acessing AuthToken')
  session = getAuthTokenSession()
  token = searchAuthToken(session, msisdn)
  print(f'3) Requesting BearerToken msisdn={msisdn}, smsToken={token}')
  bearer = requestBearerToken(msisdn, token)
  print(bearer)
  print('-----------------------------------------------------------')
  print(bearer.json()['token'])

if __name__ == "__main__":
  msisdn = sys.argv[1]
  main(msisdn)
